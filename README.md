# Roboc

Roboc est une implementation de l'activite 1 du cours d'OpenClassroom sur "Apprenez a programmer en Python".  
C'est un jeu en vue du dessus dans lequel on parcourt des labyrinths en incarnant un robot. Le but etant de sortir du labyrinth.  
Le jeu se joue en ligne de commande a l'aide des inputs suivant :
- wens L'une de ces lettres indique la direction du mouvement (w = West, e = Est, n = Nord et S = Sud).
- La direction peut etre suivie d'un nombre indiquant le nombre de pas que l'on souhaite effectuer. Par exemple n10 effectueras 10 pas vers le nord jusqu'au prochain obstacle rencontre.
- q est egalement une commande du jeu permettant de quitter. Le jeu sauvegarde automatiquement a chaque action. Il est possible de recuperer sa sauvegarde en debut de jeu.

## L'architecture

Concernant l'architecture du jeu j'ai pense a cree plusieurs class dans plusieurs package de sorte a ce qu'il soit facilement ouvert au developpement de nouvelles features.  
Roboc.py est le fichier de lancement, il sert a charger les class et a executer le jeu.  
Le package game gere les inputs et le deroulement du jeu. La class GameController est la classe principale du deroulement du jeu c'est elle qui determine comment un tour se joue. Ce qu'il faut sauvegarder ou charger. C'est un peu le chef d'orchestre du jeu.  
Le package maps gere l'affichage du jeu il contient les class Labyrinth et Carte qui servent a parser les cartes, a les modifier et pouvoir les afficher. Le labyrinth sert surtout a repertorier les obstacles et sorties. Bref les objets avec lesquels le joueur peut interagir. Mais attention c'est la class GameController qui decide des regles d'interactions entre les objets.  
Enfin le package player contient les Class Actions et Player. La class Action sert juste a repertorier les actions possible du jeu, elle n'est pas indispensable et sert surtout de memo et de medium pour interagir avec les actions du jeu. Encore une fois c'est la class Game_controller qui interprete les actions selon les regles du jeu. La class Player est centrale. Elle determine la position du joueur mais surtout les actions qu'il a enregistre. Etant donne que c'est un jeu tour par tour on charge les actions du joueurs, comme des mouvements ou le fait de quitterm dans une pile implementee a l'interieur de la class. Le joueur ne peut se deplacer que d'une seule case par tour mais il est possible de charger N mouvements vers une direction grace a la methode player.move().  

Voila tout, amusez vous bien :)
