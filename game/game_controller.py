"""
A file that contains the GameController class.
"""

import pickle
import game.inputs
import os
from player.actions import Actions
from player.player import Player
from maps.symbols import Symbols

class GameController:
    """
    That the main class of our game. This class controle the game rules and therefore the game flow.
    This Game is a round by round game.
    Run run() methods to launch the game.
    """

    def __init__(self, labyrinth, player):
        self.labyrinth = labyrinth
        self.player = player
        #On place le joueur sur la map.
        #Mais avant on erase la position de base sur la map, elle nous sert juste a indiquer le spawn de base du joueur.
        self.labyrinth.map[self.labyrinth.map.getpos(Symbols.PLAYER.value, False)[0]] = Symbols.EMPTY.value
        self.player.onfoot = self.labyrinth.map[self.player.pos]
        self.labyrinth.map[self.player.pos] = self.player.skin
        

    def save(self, file):
        """
        Save the player itself in the specified file
        """
        with open(os.path.join("cartes", f"{file}.sav"), "wb") as f:
            tmp = pickle.Pickler(f)
            tmp.dump(self.player)
    
    def load(file):
        """
        Load and returns the player pos etc from a file
        """
        with open(os.path.join("cartes", f"{file}.sav"), "rb") as f:
            tmp = pickle.Unpickler(f)
            return tmp.load()
    
    def delsave(self, file):
        """
        Just delete the save.
        """
        os.remove(os.path.join("cartes", f"{file}.sav"))

    def parseprompt(self, prompt):
        """
        Just parse the prompt and load the actions in the player.actions
        """
        error = True
        while error:
            action = input(prompt)
            if len(action) > 0:
                #If it's a movement load it.
                if action[0].lower() in "wens":
                    if action[0].lower() in "we":
                        horizontal = True
                    else:
                        horizontal =  False

                    if len(action) > 1:
                        if action[1:].isdigit():
                            direction = int(action[1:])
                            #To avoid charging 10000000 actions that cost process time for nothing.
                            if direction > self.labyrinth.map.x_len:
                                direction = self.labyrinth.map.x_len
                            elif direction > self.labyrinth.map.y_len:
                                direction = self.labyrinth.map.y_len

                            if action[0].lower() == 'w' or action[0].lower() == 'n':
                                direction = -direction
                            self.player.move(direction, horizontal)
                            error = False
                    else:
                        if action[0].lower() == 'w' or action[0].lower() == 'n':
                            self.player.move(-1, horizontal)
                        else:
                            self.player.move(1, horizontal)
                        error = False
                
                #Load the quit action if it's an action.
                elif action[0].lower() == 'q' and len(action) == 1:
                    self.player.add_action(Actions.quit())
                    error = False

            if error == True:
                print("Vous pouvez quitter avec  'q' ou decider de bouger avec 'w, e, n, s'[nombre]. Suivez juste ce type de format.")
    
    def round(self):
        """
        Define how a round work
        """
        #Check the next action of the player.
        #If the player encounter an obstacle consider that he has not performing action. So clear all actions of the player.
        #If the player has not action registered ask him his next action(s).
        perform_valid_action = False
        while not perform_valid_action:
            if self.player.has_action():
                action, pos = self.player.get_action()
                if action == "move":
                    if not self.labyrinth.iscollide(pos):
                        #Save that is on foot of the players and put that was on his foot. It's like a temporal swap!
                        self.labyrinth.map[self.player.pos] = self.player.onfoot
                        self.player.onfoot = self.labyrinth.map[pos]
                        #Place the player and update his pos.
                        self.labyrinth.map[pos] = self.player.skin
                        self.player.pos = pos
                        perform_valid_action = True
                    else:
                        print("Il y a un obstacle en face de vous !")
                        self.player.clr_actions()
                elif action == "quit":
                    self.save(self.labyrinth.map.name)
                    return 0
            else:
                self.parseprompt("> ")
                print()
        print(f"{self.labyrinth.map}\n")
        self.save(self.labyrinth.map.name)
        return 1

    def run(self):
        """
        Just run the game
        """
        play = 1
        print(self.labyrinth.map, "\n")
        while self.player.onfoot != Symbols.EXIT.value and play != 0:
            play = self.round()

        if self.player.onfoot == Symbols.EXIT.value:
            print("Felicitation vous avez gagne !")
            self.delsave(self.labyrinth.map.name)
        else:
            print("A la revoyure !")