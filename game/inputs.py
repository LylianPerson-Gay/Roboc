"""
This file contains all the inputs functions use into my projects.
Like getint() etc.
"""

from player.actions import Actions

def getint(input_message):
    """
    While the input collected is not an integer repeat the input.
    """

    number = input(input_message)
    while not number.isdigit():
        number = input(input_message)
    return int(number)

def getchar(input_message, charset=None):
    """
    While the input collected is not an char repeat the input.
    """
    char = ""
    in_charset = False
    while len(char) != 1 and not in_charset:
        char = input(input_message).lower()
        if charset is not None:
            if char in charset:
                in_charset = True
            else:
                in_charset = False
        else:
            in_charset = False
    return char