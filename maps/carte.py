"""
This file contains the Carte class.
"""

class Carte:
    """
    This class parsed the content of an str into a 2D map of (y, x).
    The class carte is a container that can be manipulated with the help of tuples.
    For example carte[y, x] = 'a' change the value at the pos specified.
    """

    def __init__(self, name, content):
        self.name = name
        self.map = content.splitlines()

        #Check if the map is a rectangular form.
        self.x_len = len(self.map[0])
        self.y_len = len(self.map)
        for y in self.map:
            if len(y) != self.x_len:
                raise TypeError(f"{self.x_len} differ of {len(y)} in line {y}. Please pass a rectangular form.")
    
    def __iter__(self):
        y = 0
        y_max = len(self.map)
        while y < y_max:
            for item in self.map[y]:
                yield item
            y += 1

    def pos(self):
        """
        An iterator that return the position and the item itself.
        """
        y = 0
        y_max = len(self.map)
        while y < y_max:
            for x, item in enumerate(self.map[y]):
                yield (y, x), item
            y += 1

    def getpos(self, to_found, pluriel=True):
        """
        Get the position(s) of to_found in the map.
        If pluriel is set to False, get only the 1st position
        """

        positions = []
        for pos, item in self.pos():
            if to_found == item:
                if not pluriel:
                    return [pos]
                positions.append(pos)
        
        if len(positions) == 0:
            raise ValueError(f"{to_found} not present in {self.name}")
        return positions

    def __contains__(self, to_found):
        for line in self.map:
            if to_found in line:
                return True
        return False
    
    def __getitem__(self, pos):
        return self.map[pos[0]][pos[1]]

    def __setitem__(self, pos, value):
        #We transform the line into list to udpate it (str are immutables).
        self.map[pos[0]] = list(self.map[pos[0]])
        self.map[pos[0]][pos[1]] = value
        self.map[pos[0]] = ''.join(self.map[pos[0]])

    def __str__(self):
        return '\n'.join(self.map)