"""
This file contains the labyrinth class.
"""

from maps.carte import Carte
from maps.symbols import Symbols

class Labyrinth:
    """
    This class contains all the pos of the player, obstacles, doors and exits.
    """

    def __init__(self, name, map):
        self.map = map
        self.obstacles = self.map.getpos(Symbols.OBSTACLE.value)
    
    def iscollide(self, pos):
        """
        Check if the position specified collide an obstacles
        """
        if pos in self.obstacles:
            return True
        return False

    def __str__(self):
        return self.map.__str__()