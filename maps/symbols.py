"""
This files contain just an enumeration of the symbols
"""

from enum import Enum

class Symbols(Enum):
    """
    Just an enumeration for easy modifications of the skins
    """
    EMPTY = ' '
    OBSTACLE = 'O'
    DOOR = '.'
    EXIT = 'U'
    PLAYER = 'X'