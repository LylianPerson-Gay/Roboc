"""
A file that contains the actions class
"""

class Actions:
    """
    A simple class that contains all actions that can be perform
    """

    actions = [
        "move",
        "quit"
    ]

    def __contains__(self, to_found):
        return to_found in self.actions
    
    def move(pos):
        return ("move", pos)
    
    def quit():
        return ("quit", 0)