"""
This file contains the player class wich is a mobile class that can performs some actions over tour.
"""

from player.actions import Actions
from maps.symbols import Symbols

class Player:
    """
    A mobile class that can perform some actions over tour.
    The list is in FIFO format, an action is specified with his type ("move") and a pos that describe where to perform it.
    """

    def __init__(self, name, skin, position):
        self.name = name
        self.skin = skin
        self.pos = position
        self.on_foot = Symbols.EMPTY.value
        self._actions = []
    
    def add_action(self, action):
        self._actions.insert(0, action)
    
    def get_action(self):
        return self._actions.pop(-1)

    def del_action(self):
        del self._actions[-1]
    
    def clr_actions(self):
        while self.has_action():
            del self._actions[-1]
    
    def has_action(self):
        if len(self._actions) == 0:
            return False
        return True

    def move(self, dir, horizontal=True):
        """
        Move in a horizontal axis if horizontal == True.
        If dir is negative go back.
        Update the list of actions.
        """
        actual_pos = self.pos
        if horizontal:
            while dir != 0:
                if dir < 0:
                    actual_pos = (actual_pos[0], actual_pos[1] - 1)
                    self.add_action(Actions.move(actual_pos))
                    dir += 1
                else:
                    actual_pos = (actual_pos[0], actual_pos[1] + 1)
                    self.add_action(Actions.move(actual_pos))
                    dir -= 1
        else:
            while dir != 0:
                if dir < 0:
                    actual_pos = (actual_pos[0] - 1, actual_pos[1])
                    self.add_action(Actions.move(actual_pos))
                    dir += 1
                else:
                    actual_pos = (actual_pos[0] + 1, actual_pos[1])
                    self.add_action(Actions.move(actual_pos))
                    dir -= 1

        def __str__(self):
            return self.skin

    actions = property(fget=get_action, fset=add_action, fdel=del_action)