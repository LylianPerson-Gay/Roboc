#!/usr/bin/python3
#coding: utf-8

import os
import game.inputs as inputs
from maps.carte import Carte
from maps.labyrinth import Labyrinth
from maps.symbols import Symbols
from player.player import Player
from game.game_controller import GameController 

"""
This file contains the main code of Roboc project.
A game where you incarnate a lost Robot in a labyrinth.
This project has to be run in a terminal with Python3
"""

filenames = []
saves = []
#1st part listing maps and displays it.
print("Labyrinths existants:")
for filename in os.listdir("cartes"):
    if filename.endswith(".txt"):
        filenames.append(filename[:-4].lower())
    elif filename.endswith(".sav"):
        saves.append(filename)

filenames.sort()
saves.sort()
for i, filename in enumerate(filenames):
    print(f"  {i + 1} - {filename}.")
print()

#Now time to choose and load the map.
choice = 0
while choice > len(filenames) or choice < 1:
    choice = inputs.getint("Entrez un numero de labyrinth pour commencer a jouer : ")
    print()

#We check if there is already a save.
wantsave = False
for save in saves:
    if save[:-4] == filenames[choice - 1]:
      if inputs.getchar("Il y a deja une sauvegardes presentes sur cette partie. Voulez-vous la charger ? (y/n) ", "yn") == 'y':
          wantsave = True

#We load the save or a new game in function of choice.
with open(os.path.join("cartes", f"{filenames[choice - 1]}.txt"), "r") as f:
    carte = Carte(filenames[choice - 1], f.read())

#Now we load the player and the labyrinth before loading the Game.
if wantsave:
    player = GameController.load(filenames[choice - 1])
else:
    player = Player("John Doe", Symbols.PLAYER.value, carte.getpos(Symbols.PLAYER.value, False)[0])

labyrinth = Labyrinth(carte.name, carte)
game = GameController(labyrinth, player)

#And we launch it!
game.run()